# Indicium Tech SQL style guide

  - [General guidelines and SQLFluff](#General-guidelines-and-SQLFluff)
  - [Example of sql code](#example-of-sql-code)
  - [Rules configuration](#rules-configuration)
  - [Credits](#credits)

# SQLFluff Version

In order to take full advantage of this SQL Style Guide, there is a specific version recommended for SQLFluff package to match the same rules defined here. The version of SQLFluff recommended is [1.4.5](https://docs.sqlfluff.com/en/1.4.5/). This guide will be updated accordingly as long as SQLFluff updates with features that are interesting to apply in our codestyle.

# General guidelines and SQLFluff
To avoid the lack of update of this documentation, we addressed the responsability of handling part of the code style to [SQLFluff](https://docs.sqlfluff.com/en/1.4.5/), a common linter of SQL in data market. Knowing that, you will find the guidelines of sql style guide based in the [SQLFluff rules](https://docs.sqlfluff.com/en/1.4.5/rules.html) and the [sqlfluff config](https://docs.sqlfluff.com/en/1.4.5/configuration.html). There are some configurations about the rules that will be specified here. The remaining rules that are not discussed here, will be used the default behavior of SQLFluff. Since this linter is constantly updating, some rules can be included or change the default behavior some time. When this happens, we are going to revisit the rules (every 6 months or so) to reinforce the rules here described.

The recommendations that are **not covered** will be separated in another doc called sql best practices. Even then, the CI pipeline will not fail when the recommendations are not met but will be pointed out by the reviewer when is not followed.

### Example of sql code
Here's a non-trivial query to give you an idea of what this style guide looks like in the practice:

```sql
with
    hubspot_interest as (
        select
            email
            , timestamp_millis(property_beacon_interest) as expressed_interest_at
        from hubspot.contact
        where property_beacon_interest is not null
    ) 

    , support_interest as (
        select 
            email
            , created_at as expressed_interest_at
        from helpscout.conversation
        inner join helpscout.conversation_tag
            on conversation.id = conversation_tag.conversation_id
        where tag = 'beacon-interest'
    )

    , combined_interest as (
        select *
        from hubspot_interest
        union all
        select *
        from support_interest
    )

    , final as (
        select 
            email
            , min(expressed_interest_at) as expressed_interest_at
        from combined_interest
        group by email
    )

select *
from final

```

# Rules configuration

## Lines should ideally not be longer than 100 characters.
Very long lines are harder to read, especially in situations where space may be limited like on smaller screens or in side-by-side version control diffs.

## Tabs should be 4 spaces.
There is a way of configuring your text editor to apply 4 spaces instead of tab in your code. Sometimes the git platform interpret the tabs diferent than spaces.

## CTEs should be indented.
```sql
/* Good */
with 
    customers as (
        select *
        from customers
    )

    , stores as (
        select *
        from stores
    )

/* Bad */
with 
customers as (
        select *
        from customers
)

, stores as (
        select *
        from stores
)
```

## `Joins` should be in the same indentation of `from` clause.
```sql
/* Good */
select *
from boo
left join faa

/* Bad */
select *
from boo
    left join faa
```

## Always indent the `on` clause from `joins`.

```sql
/* Good */
select *
from boo
left join faa
    on boo.id = faa.id
    and boo.hash = faa.hash

/* Bad */
select *
from boo
left join faa on boo.id = faa.id
    and boo.hash = faa.hash

/* Bad */
select *
from boo
left join faa on boo.id = faa.id and boo.hash = faa.hash

/* Bad */
select *
from boo
left join faa
on boo.id = faa.id
and boo.hash = faa.hash
```

## Keywords and function names should all be lowercase.
Lowercase is more readable than uppercase, and you won't have to constantly be holding down a shift key. 

```sql
/* Good */
select *
from customers

/* Good */
select count(1) as customers_count
from customers

/* Bad */
SELECT *
FROM customers

/* Bad */
Select *
From customers

/* Bad */
select COUNT(1) as customers_count
from customers
```

## Always use the `as` keyword when aliasing columns, expressions, and tables.
```sql
/* Good */
select count(1) as customers_count
from customers

/* Bad */
select count(1) customers_count
from customers
```

## Use single quotes for strings.
Some SQL dialects like BigQuery support using double quotes or even triple quotes for strings, but for most dialects:
  - Double quoted strings represent identifiers.
  - Triple quoted strings will be interpreted like the value itself contains leading and trailing single quotes.

```sql
/* Good */
select *
from customers
where email like '%@domain.com'

/* Bad */
select *
from customers
where email like "%@domain.com"
/* Will probably result in an error like `column "%@domain.com" does not exist`. */

/* Bad */
select *
from customers
where email like '''%@domain.com'''
/* Will probably be interpreted like '\'%domain.com\''. */
```

## Never use reserved words as identifiers.
Otherwise the identifier will have to be quoted everywhere it's used.

```sql
/* Good */
select name as customer_name
from customers

/* Good */
select date as delivery_date
from deliveries

/* Bad */
select name
from customers

/* Bad */
select count(1) as count
from sales
```

## Use `coalesce` instead of `ifnull` or `nvl`.
  - `coalesce` is universally supported, whereas Redshift doesn't support `ifnull` and BigQuery doesn't support `nvl`.
  - `coalesce` is more flexible and accepts an arbitrary number of arguments.

```sql
/* Good */
select coalesce(name, fantasy_name) as customer_name
from customers

/* Bad */
select ifnull(name, fantasy_name) as customer_name
from customers

/* Bad */
select nvl(name, fantasy_name) as customer_name
from customers
```

## Always alias grouping aggregates and other column expressions.
```sql
/* Good */
select max(id) as max_customer_id
from customers

/* Bad */
select max(id)
from customers
```

## Use `inner join` instead of just `join`.
It's better to be explicit so that the join type is crystal clear.

```sql
/* Good */
select *
from customers
inner join orders on customers.id = orders.customer_id

/* Bad */
select *
from customers
join orders on customers.id = orders.customer_id
```

## Use CTEs rather than subqueries. 
CTEs will make your queries more straightforward to read/reason about, can be referenced multiple times, and are easier to adapt/refactor later.

```sql
/* Good */
with
    paying_customers as (
        select *
        from customers
        where plan_name != 'free'
    )
select ...
from paying_customers

/* Bad */
select ...
from (
    select *
    from customers
    where plan_name != 'free'
) as paying_customers
```

## When joining multiple tables, always prefix the column names with the table name/alias.
You should be able to tell at a glance where a column is coming from.

```sql
/* Good */
select
    customers.email
    , orders.invoice_number
    , orders.total_amount
from customers
inner join orders on customers.id = orders.customer_id

/* Bad */
select
    email
    , invoice_number
    , total_amount
from customers
inner join orders on customers.id = orders.customer_id
```

## Never end a line with an operator like `and`, `or`, `+`, `||`, etc.
If code containing such operators needs to be split across multiple lines, put the operators at the beginning of the subsequent lines.
  - You should be able to scan the left side of the query text to see the logic being used without having to read to the end of every line.
  - The operator is only there for/because of what follows it.  If nothing followed the operator it wouldn't be needed, so putting the operator on the same line as what follows it makes it clearer why it's there.

```sql
/* Bad */
select *
from customers
where
    email like '%@domain.com' and
    plan_name != 'free'

/* Good */
select *
from customers
where
    email like '%@domain.com'
    and plan_name != 'free'
```

## Using leading commas.
If code containing commas needs to be split across multiple lines, put the commas at the beginning of the subsequent lines, followed by a space.
  - This makes it easier to spot missing commas.
  - Version control diffs will be cleaner when adding to the end of a list because you don't have to add a trailing comma to the preceding line.
  - The comma is only there for/because of what follows it.  If nothing followed the comma it wouldn't be needed, so putting the comma on the same line as what follows it makes it clearer why it's there.

```sql
/* Good */
with
    customers as (
        ...
    )
    , paying_customers as (
        ...
    )
select
    id
    , email
    , date_trunc('month', created_at) as signup_month
from paying_customers
where email in (
        'user-1@example.com'
        , 'user-2@example.com'
        , 'user-3@example.com'
    )

/* Bad */
with
    customers as (
        ...
    ),
    paying_customers as (
        ...
    )
select
    id,
    email,
    date_trunc('month', created_at) as signup_month
from paying_customers
where email in (
        'user-1@example.com',
        'user-2@example.com',
        'user-3@example.com'
    )
```

## Prefer using count(*) instead of count(1) or count(0).
You can understand more about the motivation of this choice [here](https://stackoverflow.com/questions/1221559/count-vs-count1-sql-server#:~:text=There%20is%20no%20difference.,-Reason%3A&text=%221%22%20is%20a%20non%2D,the%20same%20as%20COUNT(*)%20.).

```sql
/* Good */
select count(1) as qtd

/* Bad */
select count(*) as qtd

/* Bad */
select count(0) as qtd
```

## `select` clause:
  - If there is only one column expression, put it on the same line as `select`.
  - If there are multiple column expressions, put each one on its own line (including the first one), indented one level more than `select`.
  - If there is a `distinct` qualifier, put it on the same line as `select`.

```sql
/* Good */
select id

/* Good */
select
    id
    , email

/* Bad */
select id, email

/* Bad */
select id
    , email

/* Good */
select distinct country

/* Good */
select distinct
    state
    , country

/* Bad */
select distinct state, country
```

## Credits

This style guide was inspired in part by:
  - [Brooklyn Data's dbt coding conventions](https://github.com/brooklyn-data/co/blob/master/sql_style_guide.md)
  - [Fishtown Analytics' dbt coding conventions](https://github.com/fishtown-analytics/corp/blob/b5c6f55b9e7594e1a1e562edf2378b6dd78a1119/dbt_coding_conventions.md)
  - [Matt Mazur's SQL style guide](https://github.com/mattm/sql-style-guide/blob/3eaef3519ca5cc7f21feac6581b257638f9b1564/README.md)
  - [GitLab's SQL style guide](https://about.gitlab.com/handbook/business-ops/data-team/sql-style-guide/)
  - [SQLFluff rules](https://docs.sqlfluff.com/en/1.4.5/rules.html)
