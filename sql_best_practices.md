# Indicium Tech SQL best practices

  - [Considerations](#considerations)
  - [General guidelines](#general-guidelines)
  - [Joins](#joins)
  - [Syntax](#sintax)
  - [Window functions](#window-functions)
  - [CTE](#ctes)
  - [Credits](#credits)

# Considerations

This document is a continuation are the [sql_style_guide](sql_style_guide.md) with guidelines that are considered best practices to work with sql in any project but are **NOT COVERED** by SQLFluff rules at the moment.

# General guidelines

## Optimize primarily for readability, maintainability, and robustness rather than for fewer lines of code

Newlines are cheap, people's time is expensive.

## Avoid having enormous `select` statements

If a `select` statement is so large it can't be easily comprehended, it would be better to refactor it into multiple smaller CTEs that are later joined back together.

## Identifiers such as aliases and CTE names should be in lowercase `snake_case`

It's more readable, easier to keep consistent, and avoids having to quote identifiers due to capitalization, spaces, or other special characters.

```sql
/* Good */
select
    count(*) as total_amount
from orders

/* Bad */
select
    count(*) as Total_Amount
from orders

/* Bad */
select
    count(*) as TotalAmount
from orders
```

## Never use tab characters

It's easier to keep things consistent in version control when only space characters are used. There is a way to configure most text editors to change the behavior of tab to instead apply 4 spaces.

## General indentation

An overarching pattern is:
  - If there's only one thing, put it on the same line as the opening keyword.
  - If there are multiple things, put each one on its own line (including the first one), indented one level more than the opening keyword.

# Sintax

## Use `!=` instead of `<>`

`!=` reads like "not equal" which is closer to how we'd say it out loud.

```sql
/* Good */
select *
from orders
where total != 100

/* Bad */
select *
from orders
where total <> 100
```

## Use `||` instead of `concat`

`||` is a standard SQL operator, and in some databases like Redshift `concat` only accepts two arguments.

```sql
/* Good */
select
    city || street || postal_code
from region

/* Bad */
select
    concat(concat(city, street), postal_code)
from region
```

## Use `is null` instead of `isnull`, and `is not null` instead of `notnull`
`isnull` and `notnull` are specific to Redshift.

## Use a `case` statement instead of `iff` or `if`
`case` statements are universally supported, whereas Redshift doesn't support `iff`, and in BigQuery the function is named `if` instead of `iff`.

## Use `where` instead of `having` when either would suffice
Queries filter on the `where` clause earlier in their processing, so `where` filters are more performant.

## Use `union all` instead of `union` unless duplicate rows really do need to be removed
`union all` is more performant because it doesn't have to sort and de-duplicate the rows.

## Use `select distinct` instead of grouping by all columns
This makes the intention clear.

```sql
/* Good */
select distinct
    customer_id
    , date_trunc('day', created_at) as purchase_date
from orders

/* Bad */
select
    customer_id
    , date_trunc('day', created_at) as purchase_date
from orders
group by 1, 2
```

## Avoid using an `order by` clause unless it's necessary to produce the correct result
There's no need to incur the performance hit.  If consumers of the query need the results ordered they can normally do that themselves.

```sql
/* Good */
select *
from orders

/* Bad */
select *
from orders
order by order_date
```

## For functions that take date part parameters, specify the date part as a string rather than a keyword
  - While some advanced SQL editors can helpfully auto-complete and validate date part keywords, if they get it wrong they'll show superfluous errors.
  - Less advanced SQL editors won't syntax highlight date part keywords, so using strings helps them stand out.
  - Using a string makes it unambiguous that it's not a column reference.

```sql
/* Good */
date_trunc('month', created_at)

/* Bad */
date_trunc(month, created_at)
```

## Always use `/* */` comment syntax
This allows single-line comments to naturally expand into multi-line comments without having to change their syntax.

When expanding a comment into multiple lines:
  - Keep the opening `/*` on the same line as the first comment text and the closing `*/` on the same line as the last comment text.
  - Indent subsequent lines by 4 spaces, and add an extra space before the first comment text to align it with the text on subsequent lines.

```sql
/* Good */

-- Bad

/*  Good:  Lorem ipsum dolor sit amet, consectetur adipiscing elit,
    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    Dolor sed viverra ipsum nunc aliquet bibendum enim. */

/* Bad:  Lorem ipsum dolor sit amet, consectetur adipiscing elit,
sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
Dolor sed viverra ipsum nunc aliquet bibendum enim. */

-- Bad:  Lorem ipsum dolor sit amet, consectetur adipiscing elit,
-- sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
-- Dolor sed viverra ipsum nunc aliquet bibendum enim.
```

## Date/time column aliases:
  - Date columns based on UTC should be named like `<event>_date`.
  - Date columns based on a specific timezone should be named like `<event>_date_<timezone indicator>` (e.g. `order_date_et`).
  - Date+time columns based on UTC should be named like `<event>_at`.
  - Date+time columns based on a specific timezone should be named like `<event>_at_<timezone indicator>` (e.g `created_at_pt`).
  - US timezone indicators:
    - `et` = Eastern Time.
    - `ct` = Central Time.
    - `mt` = Mountain Time.
    - `pt` = Pacific Time.

## Boolean column aliases:
  - Boolean columns should be prefixed with a present or past tense third-person singular verb, such as:
    - `is_` or `was_`.
    - `has_` or `had_`.
    - `does_` or `did_`.

## Avoid using unnecessary table aliases, especially initialisms
Suggested guidelines:
  - If the table name consists of 3 words or less don't alias it.
  - Use a subset of the words as the alias if it makes sense (e.g. if `partner_shipments_order_line_items` is the only line items table being referenced it could be reasonable to alias it as just `line_items`).

```sql
/* Good */
select
    customers.email
    , orders.invoice_number
from customers
inner join orders on customers.id = orders.customer_id

/* Bad */
select
    c.email
    , o.invoice_number
from customers as c
inner join orders as o on c.id = o.customer_id
```

## `from` clause:
  - Put the initial table being selected from on the same line as `from`.
  - If there are other tables being joined:
    - Put each `join` on its own line, at the same indentation level as `from`.
    - If there are multiple join conditions, put each condition on its own line (including the first one), indented one level more than the `join`.

```sql
/* Good */
from customers

/* Good */
from customers
left join orders on customers.id = orders.customer_id

/* Good */
from customers
left join orders
    on customers.id = orders.customer_id

/* Good */
from customers
left join orders
    on customers.id = orders.customer_id
    and orders.total_amount > 100

/* Bad */
from customers
    left join orders on customers.id = orders.customer_id

/* Bad */
from customers
left join orders on
    customers.id = orders.customer_id
    and orders.total_amount > 100

/* Bad */
from customers
left join orders on customers.id = orders.customer_id
    and orders.total_amount > 100

```

## `where` clause:
  - If there is only one condition, put it on the same line as `where`.
  - If there are multiple conditions, put each one on its own line (including the first one), indented one level more than `where`.

```sql
/* Good */
where email like '%@domain.com'

/* Good */
where
    email like '%@domain.com'
    and plan_name != 'free'

/* Bad */
where email like '%@domain.com' and plan_name != 'free'

/* Bad */
where email like '%@domain.com'
    and plan_name != 'free'
```

## `group by` and `order by` clauses:
  - If grouping/ordering by column names/aliases:
    - If there is only one column, put it on the same line as `group by`/`order by`.
    - If there are multiple columns, put each on its own line (including the first one), indented one level more than `group by`/`order by`.

```sql

/* Good */
order by plan_name

/* Good */
order by
    plan_name
    , signup_month

/* Bad */
order by plan_name, signup_month

/* Bad */
order by plan_name
    , signup_month
```

## `in` lists:
  - Break long lists of `in` values into multiple indented lines with one value per line.

```sql
/* Good */
where email in (
        'user-1@example.com'
        , 'user-2@example.com'
        , 'user-3@example.com'
    )

/* Bad */
where email in ('user-1@example.com', 'user-2@example.com', 'user-3@example.com')
```

## Don't put extra spaces close to parentheses
```sql
/* Bad */
select *
from customers
where plan_name in ( 'monthly', 'yearly' )

/* Good */
select *
from customers
where plan_name in ('monthly', 'yearly')
```

# Joins
## Don't use `using` in joins
  - Having all joins use `on` is more consistent.
  - If additional join conditions need to be added later, `on` is easier to adapt.
  - `using` can produce inconsistent results with outer joins in some databases.

```sql
/* Good */
select *
from orders
left join orders_details
    on orders.id = orders_details.id

/* Good */
select *
from orders
left join orders_details
    on orders.id = orders_details.id
    and orders.order_date = orders_details.order_date

/* Bad */
select *
from orders
left join customers using(id)
```

## In join conditions, put the table that was referenced first immediately after `on`
This makes it easier to determine if the join is going to cause the results to fan out.

```sql
/* Good */
select *
from customers
left join orders on customers.id = orders.customer_id
/* primary key = foreign key --> one-to-many --> fan out */

/* Good */
select *
from orders
left join customers on orders.customer_id = customers.id
/* foreign key = primary key --> many-to-one --> no fan out */

/* Bad */
select *
from customers
left join orders on orders.customer_id = customers.id
```


# Window functions:
  - You can put a window function all on one line if it doesn't cause the line's length to be too long.
  - If breaking a window function into multiple lines:
    - Put each sub-clause within `over ()` on its own line, indented one level more than the window function:
      - `partition by`
      - `order by`
      - `rows between` / `range between`
    - Put the closing `over ()` parenthesis on its own line at the same indentation level as the window function.

```sql
/* Good */
select
    customer_id
    , invoice_number
    , row_number() over (partition by customer_id order by created_at) as order_rank
from orders

/* Good */
select
    customer_id
    , invoice_number
    , row_number() over (
        partition by customer_id
        order by created_at
      ) as order_rank
from orders

/* Bad */
select
    customer_id
    , invoice_number
    , row_number() over (partition by customer_id
                         order by created_at) as order_rank
from orders
```

# CTEs

  - Where performance permits, CTEs should perform a single, logical unit of work.
  - CTE names should be as verbose as needed to convey what they do.
  - CTE names should not be prefixed or suffixed with `cte`.
  - CTEs should be commented.
  - Put all closing parentheses at the same indentation level as the first CTE.
  - Put any comments about the CTE within the CTE's parentheses, at the same indentation level as the `select`.

```sql
/* Good */
with
    paying_customers as (
        select ...
        from customers
    )
    , paying_customers_per_month as (
        /* CTE comments... */
        select ...
        from paying_customers
    )
select ...
from paying_customers_per_month

/* Bad */
with paying_customers as (
        select ...
        from customers
    )
    /* CTE comments... */
    , paying_customers_per_month as (
        select ...
        from paying_customers
      )
select ...
from paying_customers_per_month
```

# Credits

This style guide was inspired in part by:
  - [Fishtown Analytics' dbt coding conventions](https://github.com/fishtown-analytics/corp/blob/b5c6f55b9e7594e1a1e562edf2378b6dd78a1119/dbt_coding_conventions.md)
  - [Matt Mazur's SQL style guide](https://github.com/mattm/sql-style-guide/blob/3eaef3519ca5cc7f21feac6581b257638f9b1564/README.md)
  - [GitLab's SQL style guide](https://about.gitlab.com/handbook/business-ops/data-team/sql-style-guide/)