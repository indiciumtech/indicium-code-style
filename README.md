# Indicium Tech Code Style.

This repository displays the code styles required by Indicium Tech in our projects:

  - [SQL style guide](sql_style_guide.md)
  - [dbt coding conventions](dbt_coding_conventions.md)
  - [dbt modeling feature PR template](pr_templates/dbt_feature_pr_template.md)
  - [dbt modeling develop PR template](pr_templates/dbt_develop_pr_template.md)
  - [Data engineering PR template](pr_templates/engineering_pr.md)
  - [Apps PR template](pr_templates/apps_pr.md)
