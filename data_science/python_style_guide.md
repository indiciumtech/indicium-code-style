# Codestyle

> A style guide is about consistency. Consistency with this style guide is important. Consistency within a project is more important. Consistency within one module or function is the most important. - [Guido van Rossum](https://peps.python.org/pep-0008/)

These are expected to be checked (and corrected) automatically by linting pipelines or, at the very least, raise eyebrows when reviewing code 🤨

All conventions defined in PEP8 applies, but some critical rules are highlighted as follows:

## Global

### Use 4 spaces for indentation level

Hanging indentation is a type-setting style where all the lines in a paragraph are indented except the first line

```
# This is good 👇
# Aligned with opening delimiter.
foo = long_function_name(var_one, var_two,
                         var_three, var_four)

# More indentation included to distinguish this from the rest.
def long_function_name(var_one, var_two, var_three,
                       var_four):
    print(var_one)

# Bad 👇
# Arguments on first line forbidden when not using vertical alignment.
foo = long_function_name(var_one, var_two,
    var_three, var_four)

# Further indentation required as indentation is not distinguishable.
def long_function_name(
    var_one, var_two, var_three,
    var_four):
    print(var_one)
```

Pep8 goes into much more detail, when in doubt make sure to check the session on indentation

### Variable and function names should be lowercase where different words are separated by an underscore

```
my_variable = 5

def my_function(a, b):
   return a + b
```
If a variable is intended to be constant throughout the program, then you can write it in uppercase to indicate this (MY_VARIABLE).

In general, you want to try and avoid having your variable names being just one letter or number. However, there are three letters that you absolutely need to avoid if, for whatever reason, you decide you are going to use a one letter variable. Those letters are ‘O’ (can be confused with the number ‘0’), ‘I’ (can be confused with the letter ‘l’), and ‘l’ (can be confused with the number ‘1’)

### Class names should be written with capitalized words joined together
```
class MyAwesomeClass():
```

### Document your functions with docstrings

Documenting your Python code is all centered on docstrings. These are built-in strings that, when configured correctly, can help other contributers (and yourself!) with the project’s documentation. Docstring conventions are described within PEP 257, make sure to check it out when in doubt.

```
# Good 👇

# One liners are for really obvious cases
def kos_root():
    """Return the pathname of the KOS root directory."""
    global _kos_root
    if _kos_root: return _kos_root
    ...

# Multi-line docstrings
def complex(real=0.0, imag=0.0):
    """Form a complex number.

    Keyword arguments:
    real: the real part (default 0.0)
    imag: the imaginary part (default 0.0)
    """
    if imag == 0.0 and real == 0.0:
        return complex_zero


# Bad 👇
# one-line docstring should NOT be a “signature” reiterating the function/method parameters
def function(a, b):
    """function(a, b) -> list"""
```

### Handle expected errors

Even if a statement or expression is syntactically correct, it may cause an error when an attempt is made to execute it. Errors detected during execution are called exceptions, that can abnormally terminate the execution of a program.

The try...except block is used to handle exceptions in Python. Using it for statements that can malfunction prevents the execution from crashing, saves debugging time and effort, and helps define expectations and requirements for the system.

```
try:
    with open('file.txt') as file:
        read_data = file.read()

except FileNotFoundError as fnf_error:
    print(fnf_error)

```

[Read more here](https://docs.python.org/3/tutorial/errors.html)


### Use type hints

Type hints are performed using Python annotations. When added as function annotation, they help specify the type of the function’s arguments and its returned value.

```
# Good
def greeting(name: str) -> str:
    return 'Hello ' + name
```

### Never overwrite a built-in

```
a = 5
b = 2
min = 0
if a < b:
    min = a
else:
    min = b

# The code above overrides the build-in function min() with a variable min. 
# Your pipeline would fail if you try to use the built-in min() function
```

## Pandas specific

### Filtering datasets with the query method
```
# This is not bad
df.loc[(df["Year"]) < 1995 & (df["Country"] == "BR")]

# This is better
df.query("Year < 1995 and Country = 'BR'")
```

### Use the `copy()` method to avoid SettingWithCopyWarning
```
# This will throw SettingWithCopyWarning
brazilians = df.query("Country = 'BR'")
brazilians["First Name"] = brazilians["Name"].split(' ', 1)[0]

# This is safer
brazilians = df.query("Country = 'BR'").copy()
brazilians["First Name"] = brazilians["Name"].split(' ', 1)[0]

```

### Chaining formulas is better than creating many intermediate dataframes
```
# Bad
df = pd.read_csv("people.csv")
df2 = df.query("Year < 1995 and Country = 'BR'")
d3 = df2.groupby(["Gender"])[["Age"]].mean()
df_out = df3.sort_values("Age")

# Chaining command improves readability and leverages Pandas' backend optimizations
people = pd.read_csv("people.csv")
avg_age_by_city = (
    df
    .query("Year < 1995 and Country = 'BR'")
    .groupby(["Gender"])[["Age"]].mean()
    .sort_values("Age")
)
```

### Check for merging expectations
```
# In this example, we are merging/joining a monthly sales table to a monthly sales target table. 

# This should be a 1:1 relationship. One way of checking is by comparing rows:
sales_with_goals = sales.merge(goals, on=["Month"])
len(sales_with_goals) == len(sales) # This is unsufficient as we would need to handle this issue in case of a False output  

# Use the validate parameter to check for merging expectations
sales_with_goals = sales.merge(goals, on=["Month"], validate="1:1") # This will throw a MergeError

# Don't forget to handle your errors!
try:
    sales_with_goals = sales.merge(goals, on=["Month"], validate="1:1")

except MergeError:
    
    error = {
        "error": {
            "status_code": "404",
            "detail": "Merging sales and goals generated more rows than expected. Check data sources."
        }
    }
    return error 
    
```

### When working with large datasets, save time and space with alternative file types

CSV is the most commonly used file type, but consider using [parquet](https://docs.kedro.org/en/stable/kedro.extras.datasets.pandas.ParquetDataSet.html) or [feather](https://docs.kedro.org/en/stable/kedro.extras.datasets.pandas.FeatherDataSet.html). Both are supported by pandas. 
