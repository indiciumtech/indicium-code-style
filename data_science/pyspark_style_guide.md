# Codestyle


## Referencing Columns

```
bad
df = df.select(F.lower(df.colA), F.upper(df.colB))

# good
df = df.select(F.lower(F.col('colA')), F.upper(F.col('colB')))

# better
df = df.select(F.lower('colA'), F.upper('colB'))
```

Since Spark 3.0 it’s possible to use the third form, simplifying the code.

In many situations the first style can be simpler, shorter and visually less polluted. However, there are some limitations:

If the column name has a space or other unsupported character, the bracket operator must be used;
Column expressions involving the dataframe aren’t reusable and can’t be used for defining abstract functions;
Renaming a dataframe can cause error or demand update all column references.


### Caveats

In some contexts there may be access to columns from more than one dataframe, and there may be an overlap in names.

```
df.join(df2, on=(df.key == df2.key), how='left')
```

In such cases it is fine to reference columns by their dataframe directly. You can also disambiguate joins using dataframe aliases (see more in the Joins section in this guide).

## Refactor complex logical operations

Logical operations, which often reside inside `.filter()` or `F.when()`, need to be readable. We apply the same rule as with chaining functions, keeping logic expressions inside the same code block to three (3) expressions at most. If they grow longer, it is often a sign that the code can be simplified or extracted out. Extracting out complex logical operations into variables makes the code easier to read and reason about, which also reduces bugs.

```
# bad
F.when( (F.col('prod_status') == 'Delivered') | (((F.datediff('deliveryDate_actual', 'current_date') < 0) & ((F.col('currentRegistration') != '') | ((F.datediff('deliveryDate_actual', 'current_date') < 0) & ((F.col('originalOperator') != '') | (F.col('currentOperator') != '')))))), 'In Service')
```

The code above can be simplified in different ways. To start, focus on grouping the logic steps in a few named variables. PySpark requires that expressions are wrapped with parentheses. This, mixed with actual parenthesis to group logical operations, can hurt readability. For example the code above has a redundant `(F.datediff(df.deliveryDate_actual, df.current_date) < 0)` that the original author didn't notice because it's very hard to spot.

```
# good
has_operator = ((F.col('originalOperator') != '') | (F.col('currentOperator') != ''))
delivery_date_passed = (F.datediff('deliveryDate_actual', 'current_date') < 0)
has_registration = (F.col('currentRegistration').rlike('.+'))
is_delivered = (F.col('prod_status') == 'Delivered')
is_active = (has_registration | has_operator)

F.when(is_delivered | (delivery_date_passed & is_active), 'In Service')
```

Note how the `F.when` expression is now succinct and readable and the desired behavior is clear to anyone reviewing this code. The reader only needs to visit the individual expressions if they suspect there is an error. It also makes each chunk of logic easy to test if you have unit tests in your code, and want to abstract them as functions.

## Use select statements to specify a schema contract

Doing a select at the beginning of a PySpark transform, or before returning, is considered good practice. This select statement specifies the contract with both the reader and the code about the expected dataframe schema for inputs and outputs.

Keep select statements as simple as possible. Due to common SQL idioms, allow only one function from spark.sql.function to be used per selected column, plus an optional `.alias()` to give it a meaningful name.

Expressions involving more than one dataframe, or conditional operations like `.when()` are discouraged to be used in a select, unless required for performance reasons.

```
# bad
aircraft = aircraft.select(
    'aircraft_id',
    'aircraft_msn',
    F.col('aircraft_registration').alias('registration'),
    'aircraft_type',
    F.avg('staleness').alias('avg_staleness'),
    F.col('number_of_economy_seats').cast('long'),
    F.avg('flight_hours').alias('avg_flight_hours'),
    'operator_code',
    F.col('number_of_business_seats').cast('long'),
)
```

Unless order matters to you, try to cluster together operations of the same type.

```
# good
aircraft = aircraft.select(
    'aircraft_id',
    'aircraft_msn',
    'aircraft_type',
    'operator_code',
    F.col('aircraft_registration').alias('registration'),
    F.col('number_of_economy_seats').cast('long'),
    F.col('number_of_business_seats').cast('long'),
    F.avg('staleness').alias('avg_staleness'),
    F.avg('flight_hours').alias('avg_flight_hours'),
)
```

The `select()` statement redefines the schema of a dataframe, so it naturally supports the inclusion or exclusion of columns, as well as the redefinition of pre-existing ones. By centralising all such operations in a single statement, it becomes much easier to identify the final schema, which makes code more readable. It also makes code more concise.

Instead of calling `withColumnRenamed()`, use aliases:

```
#bad
df.select('key', 'comments').withColumnRenamed('comments', 'num_comments')

# good
df.select('key', F.col('comments').alias('num_comments'))
```

Instead of using `withColumn()` to redefine type, cast in the select:

```
# bad
df.select('comments').withColumn('comments', F.col('comments').cast('double'))

# good
df.select(F.col('comments').cast('double'))
```

## Empty columns

If you need to add an empty column to satisfy a schema, always use `F.lit(None)` for populating that column. Never use an empty string or some other string signalling an empty value (such as NA). Beyond being semantically correct, one practical reason for using `F.lit(None)` is preserving the ability to use utilities like `isNull`, instead of having to verify empty strings, nulls, and 'NA', etc.

```
# bad
df = df.withColumn('foo', F.lit(''))

# bad
df = df.withColumn('foo', F.lit('NA'))

# good
df = df.withColumn('foo', F.lit(None))
```

## Using comments

Comments can provide useful insight into code. You should comment your code, specially if there are some rules being applied to transform or create columns or other objects. The code should be readable by itself. If you are using comments to explain the logic step by step, you should refactor it.

```
# bad

# Cast the timestamp columns
cols = ['start_date', 'delivery_date']
for c in cols:
    df = df.withColumn(c, F.from_unixtime(F.col(c) / 1000).cast(TimestampType()))
```

In the example above, we can see that those columns are getting cast to Timestamp. The comment doesn't add much value. Moreover, a more verbose comment might still be unhelpful if it only provides information that already exists in the code. For example:

```
# bad

# Go through each column, divide by 1000 because millis and cast to timestamp
cols = ['start_date', 'delivery_date']
for c in cols:
    df = df.withColumn(c, F.from_unixtime(F.col(c) / 1000).cast(TimestampType()))
```

Instead of leaving comments that only describe the logic you wrote, aim to leave comments that give context, that explain the "why" of decisions you made when writing the code. This is particularly important for PySpark, since the reader can understand your code, but often doesn't have context on the data that feeds into your PySpark transform. Small pieces of logic might have involved hours of digging through data to understand the correct behavior, in which case comments explaining the rationale are especially valuable.

```
# good

# The consumer of this dataset expects a timestamp instead of a date, and we need
# to adjust the time by 1000 because the original datasource is storing these as millis
# even though the documentation says it's actually a date.
cols = ['start_date', 'delivery_date']
for c in cols:
    df = df.withColumn(c, F.from_unixtime(F.col(c) / 1000).cast(TimestampType()))
```

## UDFs (user defined functions)

It is highly recommended to avoid UDFs, as they are dramatically less performant than native PySpark.

Sometimes you just need to create your own functions, in this case you should document your function properly, specially because you are creating rules that are probably unknown by other users. Also, the function name and arguments or variable function names should be meaningful.

```
# bad
def func(arg1):
  """some magic"""
  return result

# good
def minutes_to_hours(time_in_minutes):
  """The function converts a time variable from minutes to hours.

    Parameters:
      time_in_minutes (int): Time in minutes

    Returns:
      time_in_hours (float): Time in hours
    """

  time_in_hours = time_in_minutes / 60
  return time_in_hours
```

When using functions, give spaces between arguments and name the argument whenever possible.

```
# bad
func("A","B","left")

# good
func(first_arg="A", second_arg="B", how="left")
```

Also, you should use spaces between mathematical operators.

```
# bad
df = (
  df
  .withColumn("hours", (round("minutes")+0.01)/60)
)

# good
df = (
  df
  .withColumn("hours", (round("minutes") + 0.01) / 60)
)

# better
df = (
  df
  .withColumn(colName="hours", col=(round("minutes") + 0.01) / 60)
)
```

You should use spaces when creating objects.

```
# bad
df=df.select("A")

# good
df = df.select("A")
```

You don’t need to do it when naming functions arguments.

```
# good
df = (
  df
  .withColumn(colName = "hours", col = (round("minutes") + 0.01) / 60)
)

# better
df = (
  df
  .withColumn(colName="hours", col=(round("minutes") + 0.01) / 60)
)
```


## Join

Bad joins are the source of many tricky-to-debug issues. There are some things that help like specifying the how explicitly, even if you are using the default value (inner).

```
# bad
flights = flights.join(aircraft, 'aircraft_id')

# also bad
flights = flights.join(aircraft, 'aircraft_id', 'inner')

# good
flights = flights.join(aircraft, 'aircraft_id', how='inner')
```

Avoid right joins. Instead, switch the order of your dataframes and use a left join. It is more intuitive since the dataframe you are doing the operation on is the one that you are centering your join around.

```
# bad
flights = aircraft.join(flights, 'aircraft_id', how='right')

# good
flights = flights.join(aircraft, 'aircraft_id', how='left')
```

## Renaming columns to avoid collisions

In these cases, keep in mind:
It’s probably best to drop overlapping columns prior to joining if you don't need both;
In case you do need both, it might be best to rename one of them prior to joining; 
You should always resolve ambiguous columns before outputting a dataset. After the transform is finished running you can no longer distinguish them.

Also, avoid renaming all columns at once. Instead, give an alias to the whole dataframe, and use that alias to select which columns you want in the end.

```
# bad
columns = ['start_time', 'end_time', 'idle_time', 'total_time']
for col in columns:
    flights = flights.withColumnRenamed(col, 'flights_' + col)
    parking = parking.withColumnRenamed(col, 'parking_' + col)

flights = flights.join(parking, on='flight_code', how='left')

flights = flights.select(
    F.col('flights_start_time').alias('flight_start_time'),
    F.col('flights_end_time').alias('flight_end_time'),
    F.col('parking_total_time').alias('client_parking_total_time')
)

# good
flights = flights.alias('flights')
parking = parking.alias('parking')

flights = flights.join(parking, on='flight_code', how='left')

flights = flights.select(
    F.col('flights.start_time').alias('flight_start_time'),
    F.col('flights.end_time').alias('flight_end_time'),
    F.col('parking.total_time').alias('client_parking_total_time')
)
```

As a last word about joins, don't use `.dropDuplicates()` or `.distinct()` as a crutch. If unexpected duplicate rows are observed, there's almost always an underlying reason for why those duplicate rows appear. Adding `.dropDuplicates()` only masks this problem and adds overhead to the runtime.

## Window Functions

Window functions (such as `row_number()`, `sum()`, `over()`, `.lag()`, etc) operate on a set of rows defined by a window within a data set. When using window functions, always specify an explicit frame, using either row frames or range frames. If you do not specify a frame, Spark will generate one, in a way that might not be easy to predict. 

```
# bad
w1 = W.partitionBy('key')
w2 = W.partitionBy('key').orderBy('num')
# good
w3=W.partitionBy('key').orderBy('num').rowsBetween(W.unboundedPreceding, 0)
w4=W.partitionBy('key').orderBy('num').rowsBetween(W.unboundedPreceding, W.unboundedFollowing)
```

## Dealing with nulls

While nulls are ignored for aggregate functions (like `F.sum()` and `F.max()`), they will generally impact the result of analytic functions (like `F.first()` and `F.lead()`). Best to avoid this problem by enabling the ignorenulls flag:

```
df_nulls.select('key', F.first('num',
ignorenulls=True).over(w4).alias('first')).collect()
```

Also be mindful of explicit ordering of nulls to make sure the expected results are obtained:

```
w5 = W.partitionBy('key').orderBy(F.asc_nulls_first('num'))
.rowsBetween(W.currentRow, W.unboundedFollowing)
df_nulls.select('key', F.lead('num').over(w5).alias('lead')).collect()
# => [Row(key='a', lead=None), Row(key='a', lead=1), Row(key='a', lead=2), Row(key='a', lead=None)]
```

## Chaining of expressions

Avoid chaining of expressions into multi-line expressions with different types, particularly if they have different behaviours or contexts. For example- mixing column creation or joining with selecting and filtering.

```
# bad
df = (
    df
    .select('a', 'b', 'c', 'key')
    .filter(F.col('a') == 'truthiness')
    .withColumn('boverc', F.col('b') / F.col('c'))
    .join(df2, 'key', how='inner')
    .join(df3, 'key', how='left')
    .drop('c')
)
# better (seperating into steps)
# first: we select and trim down the data that we need
# second: we create the columns that we need to have
# third: joining with other dataframes

df = (
    df
    .select('a', 'b', 'c', 'key')
    .filter(F.col('a') == 'truthiness')
)

df = df.withColumn('boverc', F.col('b') / F.col('c'))

df = (
    df
    .join(df2, 'key', how='inner')
    .join(df3, 'key', how='left')
    .drop('c')
)
```

There are legitimate reasons to chain expressions together. These commonly represent atomic logic steps, and are acceptable. Apply a rule with a maximum of number chained expressions in the same block to keep the code readable. We recommend chains of no longer than 5 statements. 
If you find you are making longer chains, or having trouble because of the size of your variables, consider extracting the logic into a separate function:

```
# bad
customers_with_shipping_address = (
    customers_with_shipping_address
    .select('a', 'b', 'c', 'key')
    .filter(F.col('a') == 'truthiness')
    .withColumn('boverc', F.col('b') / F.col('c'))
    .join(df2, 'key', how='inner')
)

# also bad
customers_with_shipping_address = customers_with_shipping_address.select('a', 'b', 'c', 'key')
customers_with_shipping_address = customers_with_shipping_address.filter(F.col('a') == 'truthiness')

customers_with_shipping_address = customers_with_shipping_address.withColumn('boverc', F.col('b') / F.col('c'))

customers_with_shipping_address = customers_with_shipping_address.join(df2, 'key', how='inner')

# better
def join_customers_with_shipping_address(customers, df_to_join):

    customers = (
        customers
        .select('a', 'b', 'c', 'key')
        .filter(F.col('a') == 'truthiness')
    )

    customers = customers.withColumn('boverc', F.col('b') / F.col('c'))
    customers = customers.join(df_to_join, 'key', how='inner')
    return customers
```

Chains of more than 3 statements are prime candidates to factor into separate, well-named functions since they are already encapsulated, isolated blocks of logic.

The rationale for why we've set these limits on chaining:
Differentiation between PySpark code and SQL code. Chaining is something that goes against most other Python styling. You don’t chain in Python, you assign.
Discourage the creation of large single code blocks. These would often make more sense extracted as a named function.
It doesn’t need to be all or nothing, but a maximum of five lines of chaining balances practicality with legibility.
If you are using an IDE, it makes it easier to use automatic extractions or do code movements (i.e: cmd + shift + up in pycharm)
Large chains are hard to read and maintain, particularly if chains are nested.

## Multi-line expressions

Python doesn't support multiline expressions gracefully and the only alternatives are to either provide explicit line breaks, or wrap the expression in parentheses. You only need to provide explicit line breaks if the chain happens at the root node. For example:

```
# needs `\`
df = df.filter(F.col('event') == 'executing')\
       .filter(F.col('has_tests') == True)\
       .drop('has_tests')

# chain not in root node so it doesn't need the `\`
df = df.withColumn('safety', F.when(F.col('has_tests') == True, 'is safe')
                              .when(F.col('has_executed') == True, 'no tests but runs')
                              .otherwise('not safe'))
```

To keep things consistent, please wrap the entire expression into a single parenthesis block, and avoid using \:

```
# bad
df = df.filter(F.col('event') == 'executing')\
    .filter(F.col('has_tests') == True)\
    .drop('has_tests')

# good
df = (
  df
  .filter(F.col('event') == 'executing')
  .filter(F.col('has_tests') == True)
  .drop('has_tests')
)
```

It’s a good practice to align operators. Example:

```
# bad
df = (
  df.withColumn('A', F.col('event') * 100).withColumn('B', F.col('event') / 60)
  .drop('has_tests')
)

# good

df = (
  df
   .withColumn('A', F.col('event') * 100)
   .withColumn('B', F.col('event') / 60)
   .drop('has_tests')
)
```

## Other Considerations and Recommendations

Do not keep commented out code checked in the repository. This applies to single line of codes, functions, classes or modules. Rely on git and its capabilities of branching or looking at history instead.
Try to be as explicit and descriptive as possible when naming functions or variables. Strive to capture what the function is actually doing as opposed to naming it based the objects used inside of it.
Think twice about introducing new import aliases, unless there is a good reason to do so. Some of the established ones are types and functions from PySpark from pyspark.sql import types as T, functions as F.
Avoid using literal strings or integers in filtering conditions, new values of columns etc. Instead, to capture their meaning, extract them into variables, constants, dicts or classes as suitable. This makes the code more readable and enforces consistency across the repository.

## Imports

Avoid import all functions, this brings a lot of functions to memory and the majority will probably not be used. Also, avoid to import all functions as F for future reference, because this improve code readability making the code more clean.

```
# bad
from pyspark.sql.functions import *

df = (
  df
   .withColumn('A', col('event') * 100)
   .withColumn('B', col('event') / 60)
   .drop('has_tests')
)

# good
from pyspark.sql import functions as F

df = (
  df
   .withColumn('A', F.col('event') * 100)
   .withColumn('B', F.col('event') / 60)
   .drop('has_tests')
)

# better
from pyspark.sql.functions import col

df = (
  df
   .withColumn('A', col('event') * 100)
   .withColumn('B', col('event') / 60)
   .drop('has_tests')
)
```

## Creating columns base on conditions

When creating columns based on conditions avoid to use .otherwise and .when together, there is no need for this and improves code readability. For example:

```
# bad
df = (
  df
  .withColumn('safety', when(col('has_tests') == True, 'is safe')
                       .otherwise(when(col('has_executed') == True, 'no tests'))
                       .otherwise('not safe'))

# good
df = (
  df
  .withColumn('safety', when(col('has_tests') == True, 'is safe')
                       .when(col('has_executed') == True, 'no tests')
                       .otherwise('not safe'))
```

## Nesting functions

Avoid nesting functions, this hurts code readability.

```
# bad
df = (
    df
    .join(df2.withColumnRenammed("old","new"), 'key', how='inner')
    .join(df3.select("A","B"), 'key', how='left')
    .drop('c')
)

# good
df2 = df2.withColumnRenammed("old","new")
df3 = df3.select("A","B")

df = (
    df
    .join(df2, 'key', how='inner')
    .join(df3, 'key', how='left')
    .drop('c')
)
```


## References

Palantir style guide: https://github.com/palantir/pyspark-style-guide

