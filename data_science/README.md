# Motivation

Who cares about code quality? If it works it works, right? **Not at all!** With bad code quality, it is easy for errors and questionable edge cases to go unnoticed. This leads later down the road to time-consuming bug fixes and, at worst, production failures. By reading and following this guide, we hope that it helps you write high quality data science code which will be understandable in the future, extendable to new situations and fail early and fail fast.

This guide is structured as follows:

First, best practices are presented with a guideline on code quality expectations. You can use this as a guide to help you review code changes.

Code style rules are set forth. These are expected to be checked (and corrected) automatically by linting pipelines or, at the very least, raise eyebrows when reviewing code 🤨

Lastly, a guide on how to use black formatter for automagically linting code.

### References

* https://peps.python.org/pep-0008/
* https://docs.python.org/3/library/typing.html
* https://columbia-applied-data-science.github.io/pages/lowclass-python-style-guide.html
* https://github.com/worldbank/Python-for-Data-Science/blob/master/clean_code_guidelines_(PEP8).md
* https://martinfowler.com/bliki/Yagni.html
* https://realpython.com/documenting-python-code/#documenting-your-python-code-base-using-docstrings
* https://towardsdatascience.com/how-to-write-high-quality-python-as-a-data-scientist-cde99f582675
* https://towardsdatascience.com/12-beginner-concepts-about-type-hints-to-improve-your-python-code-90f1ba0ac49

