# Coding conventions

## Principles

A few high-level principles to help us get started:

* Keep It Simple, Stupid 😘 — simplicity should be foremost in your mind. Your code must be understood, used, and maintained by many different people, over long time periods. Adding additional complexity to your code forces these people to do unnecessary work. This is rude and causes mistakes.

* Don’t Repeat Yourself (DRY): avoiding repetition makes code more compact and robust, allows for function reuse and speeds up development. When applied correctly, making changes to a part of the system doesn't impact the overall functionality. 


> Every piece of knowledge must have a single, unambiguous, authoritative representation within a system - [The Pragmatic Programmer](https://en.wikipedia.org/wiki/The_Pragmatic_Programmer)

* You Aren’t Gonna Need It (YAGNI): Capabilities that we presume our system needs in the future should not be built now because, well, "you aren't gonna need it".

## Best practices

### Choose names that indicate in a succinct manner what the entity is (for variables and classes) or what it does (for functions)

> There are only two hard things in Computer Science: cache invalidation and naming things. — Phil Karlton

Explicit is better than implicit, but pick the length carefully: Variable names should not be too short, as they will probably not be understood by everyone, or too long, where they may become unreadable.

```
# Too short is bad
c = 7
vprice = 5

# Too long is also bad
count_of_how_many_steps_our_pipeline_has = 7 
variance_of_the_column_price_in_our_dataset = 5

# Just right
pipeline_steps = 7 
price_variance = 5
```

### Use intuitive domain language

Avoid using the datatype in the name! This is called hungarian notation and is largely discouraged for being redundant and confusing

```
# bad
list = [’apple’, ‘orange’, ‘guava’]

# bad
fruits_list = [’apple’, ‘orange’, ‘guava’]

# good
fruits = [’apple’, ‘orange’, ‘guava’] 
```

### Modularize Your Code

📖 [Extra reading - SOLID Principles](https://medium.com/backticks-tildes/the-s-o-l-i-d-principles-in-pictures-b34ce2f1e898). Kedro generally covers the *OLID*, make sure to account for the S -  The **S**ingle Responsibility Principle which states that functions (as well as classes) should have a single responsibility.

When using kedro, your code should be divided into functions, classes, nodes and pipelines. Nodes can aggregate multiple responsibilities if it makes sense in the business context, but prefer modularizing functions to adhere to a single responsibility.

When your code is grouped logically into functions you can:

* Easily **reuse** some of the code: Since a function is an independent piece you can export that specific function to other settings. Maybe you have developed a function `clean_missing_values()` for cleaning missing values in datasets. This function can be applied to both training and inferencing datasets, as well as exporting it to other projects.

* Easily check for bugs in your code: Functions can be tested for bugs through **unit tests**.

* Easily add **documentation** for more clarity: With functions, you can add docstrings that explain what they do. You can also add type hints to functions that explain the data types of the inputs and outputs to other developers and data scientists. You will find that modularized code is a lot easier to understand once the complexity of projects grows.

♻️ Periodically refactor your code into functions and classes when developing

### Test your code!

Unit tests test one small thing. This should be done with kedro’s built-in test functionality. If a unit test relies on some outside data file, it is probably too big.

Integration tests test functionality of the entire pipeline at once.

The interface may be tested as part of integration tests (that can use outside files).

Integration tests can generate some output data and compare this data to benchmark data. This comparison will tell you if the output has changed.

Both are implemented for kedro base projects. Make sure to check them out as a starting point!