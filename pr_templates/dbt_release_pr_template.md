Title: Release x (version number)

### OVERVIEW

Short description of the current release. Not all topics have to be filled but make sure to address all changes being made. 

#### Added

* for new features

#### Changed

* for changes in existing functionality.

#### Deprecated

* for soon-to-be removed features.

#### Removed

* for now removed features.

#### Fixed

* for any bug fixes.

#### Security

* in case of vulnerabilities