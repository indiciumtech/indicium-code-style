## Overview
_Summarize the goal of your work and what motivated it._

## Key changes
- _Describe major updates._

## Other notes
- _(optional) List open questions and other things your reviewer should know._

## Related links
- _(optional) List links to related ticket(s)._
- _(optional) Link to other files and resources, such as specific related reports in a BI tool._

## Checklist

- [ ] I have read the [ADR004: MLOps Framework](https://wiki.indicium.tech/en/dados/data-science/ml-ops)
- [ ] All models, whether directly changed by this PR or not, run successfully.
- [ ] New code follows [DS - Python Coding Conventions and DS - Python Style Guide best practices](https://bitbucket.org/indiciumtech/indicium-code-style/src/master/data_science/).
- [ ] New models and its metrics are being registered and can be accessed for serving

Testing:
  - [ ] All notebooks added to the project can be executed succesfully from scratch, and are with emptied outputs.
  - [ ] All added/modified pipelines, nodes and functions are documented.
  - [ ] Added tests to cover changes.
  - [ ] All tests pass. **OR** [ ] This PR causes new test failures and an issue was created with details, impacts and expected behaviour.
